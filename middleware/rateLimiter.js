const { response } = require('../Helper/Service')
const redis = require('../connection/redis')
const { INVALID_ALLOWED_HITS, INVALID_SECOUNDS_WINDOW, RATE_LIMIT_EXCEED } = require('./Config')
/**
 * @param {integer} secoundsWindow 
 * @param {integer} allowedHits 
 * @param {string} rateLimitIndicator 
 * @returns Rate Limit Response
 * @next  If User have any API hit limit it will go next function
 */
function rateLimiter({ secoundsWindow, allowedHits, rateLimitIndicator = "ip" }) {
    return async function (req, res, next) {
        const ip = req.ip
        let UniqueRateLimitPoint = ip

        if (!(rateLimitIndicator === "ip")) {
            UniqueRateLimitPoint = ip + rateLimitIndicator
        }

        if (secoundsWindow) {
            if (!(Number.isInteger(secoundsWindow))) {
                return INVALID_ALLOWED_HITS
            }
        }

        if (allowedHits) {
            if (!(Number.isInteger(allowedHits))) {
                return INVALID_SECOUNDS_WINDOW
            }
        }

        secoundsWindow = secoundsWindow ? secoundsWindow : 60
        allowedHits = allowedHits ? allowedHits : 10

        const requests = await redis.incr(UniqueRateLimitPoint)

        if (requests === 1) {
            await redis.expire(UniqueRateLimitPoint, secoundsWindow)
            secoundsWindow
        } else {
            await redis.ttl(UniqueRateLimitPoint)
        }

        if (requests > allowedHits) {
            return res.status(429).json(
                response("Rate Limited", {}, true))
        } else {
            next()
        }
    }
}
module.exports = rateLimiter