module.exports.Config = {
    EMAIL: {
        USER_RFGISTERED_MAIL: {
            SUBJECT: "Please verify Mail Address"
        }
    },
    Localization: {
        EMAIL_IS_ALREADY_IN_DB: "Email is Already Registered",
        EMAIL_OR_PASSWORD_IS_NOT_IN_DB: "Email or Password Wrong",
        VERIFY_EMAIL: "Please verify email that is sent to yout mail address!",
        USER_RFGISTERED_SUCESSFULLY: "User Registered Sucessfully!",
        USER_LOGGED_IN_SUCESSFULLY: "User LoggedIn Sucessfully!",
        INVALID_INPUT: "Input Data in invalid",
    }
}