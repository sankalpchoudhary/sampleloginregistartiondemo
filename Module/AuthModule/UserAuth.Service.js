const { default: mongoose } = require('mongoose');
const Users = require('../../model/users.model')

module.exports = class UserService {
    static async add(data) {
        try {
            const UsersAddResponse = await Users.create(data)
            return UsersAddResponse
        } catch (error) {
            console.log('Error in add Users', error);
            throw new Error(error)
        }
    }

    static async findOne(data) {
        try {
            const UsersFindResponse = await Users.findOne(data)
            return UsersFindResponse
        } catch (error) {
            console.log('Error in find Users', error);
            throw new Error(error)
        }
    }

    static async updateOne(findData, updateData) {
        try {
            const UsersFindResponse = await Users.updateOne(findData, updateData)
            return UsersFindResponse
        } catch (error) {
            console.log('Error in find Users', error);
            throw new Error(error)
        }
    }
}