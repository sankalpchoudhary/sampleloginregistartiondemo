const { Validator } = require('node-input-validator');
const fs = require('fs')
const ejs = require('ejs')
const UserService = require('./UserAuth.Service');
const { Config } = require('./UserAuth.Config');
const { response, hash, checkPassword, token_generator, encrypt, randomStringGenerate } = require('../../Helper/Service');
const MailService = require('../../Helper/MailFunction');


/**
 * @param {{email:string, f_name:string,l_name:string, password:string}} req 
 * @param {{Success:Boolean,Error:Boolean,ErrorData:Object,Message:string, Data:Object}} res 
 * @returns 
 */
module.exports.registration = async (req, res) => {
    const v = new Validator(req.body, {
        email: 'required|email',
        f_name: 'required|alpha|minLength:2|maxLength:25',
        l_name: 'required|alpha|minLength:2|maxLength:25',
        password: 'required|string|minLength:8|maxLength:100',
        role: 'required|string|minLength:5|maxLength:8',
    });

    const matched = await v.check();

    if (!matched) {
        return res.status(422).json(
            response(Config.Localization.INVALID_INPUT, v.errors, true));
    }


    const { email, f_name, l_name, password, role } = req.body

    const ValidRole = ['admin', 'customer']

    const ValidateRole = ValidRole.some(Userrole => Userrole === role)

    if (!(ValidateRole)) {
        return res.status(404).json(
            response(Config.Localization.INVALID_INPUT, {
                "role": {
                    "message": "your role in invalid.",
                    "rule": "role"
                }
            }, true));
    }

    let CheckUser = await UserService.findOne({ email })

    if (CheckUser) {
        return res.status(404).json(
            response(Config.Localization.EMAIL_IS_ALREADY_IN_DB, {}, true));
    }

    const verificationString = "verifystr" + randomStringGenerate(10) + Date.now() + randomStringGenerate(10) + "md5" + randomStringGenerate(6)

    const UserAddData = {
        email, f_name, l_name, password: hash(password), role, verificationString
    }

    const VerifyRegistrationEmail = fs.readFileSync(
        `${__dirname}/MailTemplete/VerifyRegistrationEmail.ejs`,
        { encoding: "utf-8" }
    )

    const verificationUrl = `${process.env.SERVER_ENDPOINT}/users/verifyemail/${verificationString}?type=user_registration&role=${role}`

    const MailConfig = {
        to: email,
        subject: Config.EMAIL.USER_RFGISTERED_MAIL.SUBJECT,
        html: ejs.render(VerifyRegistrationEmail, { verificationUrl, type_of_action: "registration" })
    }

    MailService.sendMail(MailConfig)
    let AddUser = await UserService.add(UserAddData)

    return res.status(200).json(
        response(Config.Localization.USER_RFGISTERED_SUCESSFULLY, {
            email: AddUser?.email,
            f_name: AddUser?.f_name,
            l_name: AddUser?.l_name,
            isVerified: AddUser?.isVerified,
        }));
}

/**
 * @param {{verification_slug:string, password:string}} req 
 * @param {{Success:Boolean,Error:Boolean,ErrorData:Object,Message:string, Data:Object}} res 
 * @returns If Email and Password match then return JWT Token else Sent Error to User
 */

module.exports.login = async (req, res) => {
    const v = new Validator(req.body, {
        email: 'required|email',
        password: 'required|string|minLength:8|maxLength:100',
        role: 'required|string|minLength:5|maxLength:8',
    });

    const matched = await v.check();

    if (!matched) {
        return res.status(422).json(
            response(Config.Localization.INVALID_INPUT, v.errors, true));
    }


    const { email, password, role } = req.body

    const ValidRole = ['admin', 'customer']

    const ValidateRole = ValidRole.some(Userrole => Userrole === role)

    if (!(ValidateRole)) {
        return res.status(404).json(
            response(Config.Localization.INVALID_INPUT, {
                "role": {
                    "message": "your role in invalid.",
                    "rule": "role"
                }
            }, true));
    }

    let CheckUser = await UserService.findOne({ email, role })

    if (!(CheckUser)) {
        return res.status(404).json(
            response(Config.Localization.EMAIL_OR_PASSWORD_IS_NOT_IN_DB, {}, true));
    }

    if (!(CheckUser.isVerified)) {
        return res.status(404).json(
            response(Config.Localization.VERIFY_EMAIL, {}, true));
    }

    if (checkPassword(password, CheckUser.password)) {
        const Token = token_generator({
            role: CheckUser?.role,
            isVerified: CheckUser?.isVerified,
            email: CheckUser?.email,
            id: encrypt(CheckUser?._id.toString()),
        })
        return res.status(200).json(
            response(Config.Localization.USER_LOGGED_IN_SUCESSFULLY, {
                Token
            }));

    } else {
        return res.status(404).json(
            response(Config.Localization.EMAIL_OR_PASSWORD_IS_NOT_IN_DB, {}, true));
    }


}

/**
 * @param {{verification_slug:string}} req 
 * @param {*} res 
 * @returns
 */

module.exports.verify_registration_mail = async (req, res) => {

    const { verification_slug } = req.params
    const { role, type } = req.query

    const ValidRole = ['admin', 'customer']

    const ValidateRole = ValidRole.some(Userrole => Userrole === role)

    if (!(ValidateRole)) {
        return res.render('VerificationError')
    }

    const ValidType = ['user_registration']

    const ValidateType = ValidType.some(Userrole => Userrole === type)

    if (!(ValidateType)) {
        return res.render('VerificationError')
    }

    let CheckUser = await UserService.findOne({ verificationString: verification_slug, role })

    if (!(CheckUser)) {
        return res.render('VerificationError')
    }

    await UserService.updateOne({ verificationString: verification_slug, role }, { isVerified: true, verificationString: "" })

    return res.render('VerificationSuccess')
}

