const mongoose = require("mongoose");

const Schema = mongoose.Schema;

let LogSchema = new Schema({
    req: {
    },
    res: {
    },
}, {
    timestamps: true
});

const Logger = mongoose.model("log", LogSchema);

module.exports = Logger;