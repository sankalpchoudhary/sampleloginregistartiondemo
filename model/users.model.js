const mongoose = require("mongoose");

const Schema = mongoose.Schema;

let UsersSchema = new Schema({
    email: {
        type: String,
        required: true
    },
    role: {
        type: String,
        enum: ['admin', 'customer'],
        required: true
    },
    f_name: {
        type: String,
        required: true
    },
    l_name: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    verificationString: {
        type: String,
        required: true
    },
    isVerified: {
        type: Boolean,
        default: false
    },
    isActive: {
        type: Boolean,
        default: true
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
}, {
    timestamps: true
});

UsersSchema.index('email')

const Users = mongoose.model("users", UsersSchema);

module.exports = Users;