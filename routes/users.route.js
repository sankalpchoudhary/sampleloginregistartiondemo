var express = require('express');
var router = express.Router();
const UserAuthCtrl = require('../Module/AuthModule/UserAuth.Controller')
const rateLimiter = require('../middleware/rateLimiter')

router.post('/registration', rateLimiter({ secoundsWindow: 180, allowedHits: 5, rateLimitIndicator: "registration" }), UserAuthCtrl.registration);

router.post('/login', rateLimiter({ secoundsWindow: 180, allowedHits: 5, rateLimitIndicator: "login" }), UserAuthCtrl.login);

router.get('/verifyemail/:verification_slug', UserAuthCtrl.verify_registration_mail);

module.exports = router;