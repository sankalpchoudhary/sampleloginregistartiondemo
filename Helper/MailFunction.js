"use strict";
const nodemailer = require("nodemailer");

module.exports = class MailService {
    /**
     * @param {{to:email,subject:string,text:string,html:string,hasAttachments:boolean,filename:string, path:path}} data 
     */
    static async sendMail(data) {
        try {
            // create reusable transporter object using the default SMTP transport
            let transporter = nodemailer.createTransport({
                host: "smtp.gmail.com",
                port: 587,
                secure: false, // true for 465, false for other ports
                auth: {
                    user: process.env.EMAIL_USER_ID,
                    pass: process.env.EMAIL_PASS,
                },
            });

            // send mail with defined transport object
            let MailConfig = {
                from: `"Health E" <${process.env.EMAIL_USER_ID}>`, // sender address
                to: data.to ? data.to : process.env.EMAIL_USER_ID, // list of receivers
                subject: data.subject, // Subject line
                text: data.text, // plain text body
                html: data.html, // html body

            }

            if (data.hasAttachments) {
                MailConfig['attachments'] = {
                    filename: data.filename,
                    path: data.path,
                }
            }
            let info = await transporter.sendMail(MailConfig);

            console.log("Message sent: %s", info.messageId);


        } catch (error) {
            console.log("Error in sent mail", error);
        }
    }
}