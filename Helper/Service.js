var jwt = require("jsonwebtoken");
const crypto = require("crypto");
const bcrypt = require('bcrypt');
const randomstring = require('randomstring')

const saltRounds = 10;

const ENCRYPTON_KEY = process.env.ENCRYPTON_KEY
const INITIAL_KEY = process.env.INITIAL_KEY;
const ALGORITHM = "aes-256-cbc";


module.exports = class Service {
    static token_generator(token_data) {
        const token = jwt.sign(
            token_data,
            process.env.SECRET,
            // {
            //     expiresIn: parseInt(process.env.EXPIRE_TOKEN)
            // }
        );
        return token
    }

    static response(Message = "", Data = {}, Error = false) {
        const ResponseObject = {
            Success: true,
            Error: false,
            Message,
        }

        if (Error) {
            ResponseObject.Success = false
            ResponseObject.Error = true
            ResponseObject.ErrorData = Data
            ResponseObject.Data = {}
        } else {
            ResponseObject.Data = Data
        }
        return ResponseObject
    }

    static hash(data) {
        const salt = bcrypt.genSaltSync(saltRounds)
        return bcrypt.hashSync(data, salt)
    }

    static checkPassword(password, hash) {
        return bcrypt.compareSync(password, hash)
    }

    static randomStringGenerate(length) {
        return randomstring.generate(length)
    }

    static isEmptyObject(obj) {
        return (
            Object.getPrototypeOf(obj) === Object.prototype &&
            Object.getOwnPropertyNames(obj).length === 0 &&
            Object.getOwnPropertySymbols(obj).length === 0
        );
    }

    static encrypt(data) {
        // the cipher function
        const cipher = crypto.createCipheriv(ALGORITHM, ENCRYPTON_KEY, INITIAL_KEY);
        let encryptedData = cipher.update(data, "utf-8", "hex");
        encryptedData += cipher.final("hex");
        return encryptedData
    }

    static decrypt(data) {
        // the decipher function
        const decipher = crypto.createDecipheriv(ALGORITHM, ENCRYPTON_KEY, INITIAL_KEY);
        let decryptedData = decipher.update(data, "hex", "utf-8");
        decryptedData += decipher.final("utf8");
        return decryptedData
    }
}