require('dotenv').config();
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const mongoose = require('mongoose');
const rateLimiter = require('./middleware/rateLimiter')

mongoose.set('strictQuery', true)

var UserRouter = require('./routes/users.route');


/**
 *********** Middlewares *********
*/

/**
 *  Interceptor middleware Intercept Req Data and Response and Save it log file na DB
*/

const { Interceptor } = require('./middleware/Interceptor');
const { response } = require('./Helper/Service');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/users', Interceptor(), rateLimiter({ secoundsWindow: 60, allowedHits: 20 }), UserRouter);

// Demonstrate the readyState and on event emitters
console.log(mongoose.connection.readyState); //logs 0
mongoose.connection.on('connecting', () => {
  console.log('connecting')
  console.log(mongoose.connection.readyState); //logs 2
});
mongoose.connection.on('connected', () => {
  console.log('connected');
  console.log(mongoose.connection.readyState); //logs 1
});
mongoose.connection.on('disconnecting', () => {
  console.log('disconnecting');
  console.log(mongoose.connection.readyState); // logs 3
});
mongoose.connection.on('disconnected', () => {
  console.log('disconnected');
  console.log(mongoose.connection.readyState); //logs 0
});

// Connect to a MongoDB server
mongoose.connect(process.env.MONGO_DB_URI, {
  useNewUrlParser: true // Boilerplate for Mongoose 5.x
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  return res.status(404).json(response("Something Went Wrong", {
    "RouteNotFound": {
      message: "this route is not available",
      rule:"RouteNotFound"
    }
  }, true));
});

// error handler
app.use(function (err, req, res, next) {
  return res.status(404).json(response("Something Went Wrong", {}, true));
});

module.exports = app;