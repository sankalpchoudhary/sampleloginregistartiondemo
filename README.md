## How to Run Project
Tech Stack
1. ExpressJS
2. MongoDB
3. Radis => For rate limit

Run Project
```
npm run start

or

nodemon
```


Sample .env File
```
MONGO_DB_URI = mongodb://127.0.0.1/test
SERVER_ENDPOINT = http://127.0.0.1:3000
SALTROUNDS = 10
EXPIRE_TOKEN = 86400
EXPIRE_REFRESH_TOKEN = 86400
SECRET = vOVH6sdmpNWjRRIqCc7rdxvOVH6sdmpNWjRRIqCc7rdxs01lwHzfr3s01lwHzfr3vOVH6sdmpNWjRRIq
ENCRYPTON_KEY = 'vOVH6sdmpNWjRRIqCc7rdxs01lwHzfr3'
INITIAL_KEY = 'vOVH6sdmpNWjRRIq'
EMAIL_USER_ID = <email>
EMAIL_PASS = <password>
```
